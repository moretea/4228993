#include <stdio.h>
#include <unistd.h>
#include <signal.h>

static char keep_running;

void handler(int signal) {
  printf("Got signal %i, closing down soon!\n", signal);
  keep_running = 0;
}

int main(int argc, char *argv[]) {
  /* Setup signal handler */
  signal(SIGINT,  handler);
  signal(SIGQUIT, handler);

  keep_running = 1;
  while(keep_running == 1) {
    printf("ZZZZZ\n");
    sleep(1);
    printf("zzzzz\n");
    sleep(1);
  }

  printf("Cleaning up\n");
  /* close files / release memory */
}